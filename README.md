# Capita React coding exercise

Christian Chown / [christianchown.com](https://www.christianchown.com) / [github](https://github.com/christianchown) / [@christianchown](https://twitter.com/christianchown)

## Install / run

1. Clone this repo
2. `yarn`
3. `yarn start`

## Test

- `yarn test`
