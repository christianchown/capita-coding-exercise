import findRoutes from "./findRoutes";
import { Waypoint } from "./RoutingContext";

const unstartedWaypoint = (route: Waypoint) => !route.started;

it("finds nothing if there's no matches", () => {
  expect(
    findRoutes("a", unstartedWaypoint)([
      [{ goingTowards: "c", started: true, completed: false, distance: 10 }],
      [
        { goingTowards: "d", started: true, completed: true, distance: 10 },
        { goingTowards: "e", started: true, completed: false, distance: 5 }
      ]
    ])
  ).toEqual([]);
});

it("finds matches", () => {
  expect(
    findRoutes("a", unstartedWaypoint)([
      [{ goingTowards: "c", started: false, completed: false, distance: 10 }],
      [
        { goingTowards: "d", started: true, completed: true, distance: 10 },
        { goingTowards: "e", started: false, completed: false, distance: 5 }
      ]
    ])
  ).toEqual([
    {
      beenAt: ["a"],
      waypoint: {
        goingTowards: "c",
        started: false,
        completed: false,
        distance: 10
      }
    },
    {
      beenAt: ["a", "d"],
      waypoint: {
        goingTowards: "e",
        started: false,
        completed: false,
        distance: 5
      }
    }
  ]);
});
