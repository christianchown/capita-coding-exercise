import React, { CSSProperties } from "react";
import getPosition from "./getPosition";
import "./Connection.scss";
import { Point } from "./data";

function precisionRound(number: number, precision: number) {
  const factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}

export default function Connection({
  name,
  source,
  dest,
  distance,
  total,
  size: [width, height]
}: {
  name: string;
  source: Point;
  dest: Point;
  distance: number;
  total: number;
  size: [number, number];
}) {
  if (!width || !height) {
    return null;
  }
  const sourcePos = getPosition({ width, height, pointKey: source, total });
  const destPos = getPosition({ width, height, pointKey: dest, total });
  const centreLeft = (sourcePos.left + destPos.left) / 2;
  const centreTop = (sourcePos.top + destPos.top) / 2;
  const center: CSSProperties = {
    position: "absolute",
    left: centreLeft,
    top: centreTop
  };
  const left = sourcePos.left;
  const top = sourcePos.top;
  const diffLeft = sourcePos.left - destPos.left;
  const diffTop = sourcePos.top - destPos.top;
  const diameter = Math.sqrt(diffLeft * diffLeft + diffTop * diffTop);

  const asine =
    precisionRound(sourcePos.left, 3) === precisionRound(destPos.left, 3)
      ? 0
      : Math.asin(diffLeft / diameter);
  const angle =
    sourcePos.top > destPos.top
      ? (Math.PI * 3) / 2 - asine
      : Math.PI / 2 + asine;
  const line: CSSProperties = {
    position: "absolute",
    left,
    top,
    width: diameter,
    transform: `translate(-${diameter /
      2}px,0) rotate(${angle}rad) translate(${diameter / 2}px,0)`
  };
  return (
    <div className="connection">
      <div className="connection__line" style={line} />
      <div className="connection__distance" style={center}>
        {name} {distance}
      </div>
    </div>
  );
}
