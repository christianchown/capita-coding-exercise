import getBestRoutes from "./getBestRoutes";

it("returns [] if there's no routes", () => {
  expect(getBestRoutes("a", "b", [])).toEqual([]);
});

it("returns [] if there's no routes that end at the destination", () => {
  expect(
    getBestRoutes("a", "b", [
      [{ goingTowards: "c", started: true, completed: true, distance: 10 }],
      [
        { goingTowards: "d", started: true, completed: true, distance: 10 },
        { goingTowards: "e", started: true, completed: true, distance: 5 }
      ]
    ])
  ).toEqual([]);
});

it("returns a single match if there's a route that ends at the destination shortest", () => {
  expect(
    getBestRoutes("a", "b", [
      [{ goingTowards: "c", started: true, completed: true, distance: 10 }],
      [
        { goingTowards: "d", started: true, completed: true, distance: 10 },
        { goingTowards: "e", started: true, completed: true, distance: 5 },
        { goingTowards: "b", started: true, completed: true, distance: 5 }
      ],
      [
        { goingTowards: "f", started: true, completed: true, distance: 1 },
        { goingTowards: "b", started: true, completed: true, distance: 2 }
      ]
    ])
  ).toEqual([
    [
      { goingTowards: "f", started: true, completed: true, distance: 1 },
      { goingTowards: "b", started: true, completed: true, distance: 2 }
    ]
  ]);
});

it("returns multiple matches if there's multiple routes that ends at the destination equally shortest", () => {
  expect(
    getBestRoutes("a", "b", [
      [{ goingTowards: "c", started: true, completed: true, distance: 10 }],
      [
        { goingTowards: "d", started: true, completed: true, distance: 10 },
        { goingTowards: "e", started: true, completed: true, distance: 5 },
        { goingTowards: "b", started: true, completed: true, distance: 5 }
      ],
      [
        { goingTowards: "f", started: true, completed: true, distance: 1 },
        { goingTowards: "b", started: true, completed: true, distance: 2 }
      ],
      [
        { goingTowards: "g", started: true, completed: true, distance: 2 },
        { goingTowards: "b", started: true, completed: true, distance: 1 }
      ]
    ])
  ).toEqual([
    [
      { goingTowards: "f", started: true, completed: true, distance: 1 },
      { goingTowards: "b", started: true, completed: true, distance: 2 }
    ],
    [
      { goingTowards: "g", started: true, completed: true, distance: 2 },
      { goingTowards: "b", started: true, completed: true, distance: 1 }
    ]
  ]);
});
