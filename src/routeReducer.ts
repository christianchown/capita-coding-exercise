import { RunningRoute } from "./RoutingContext";
import { WaypointRoute } from "./findRoutes";
import { Point, points, Distance } from "./data";

interface ResetAction {
  type: "RESET";
}
interface InitAction {
  type: "INIT";
  routes: RunningRoute[];
}

interface StartAction {
  type: "START";
  unstartedRoute: WaypointRoute;
}

interface EndAction {
  type: "END";
  unstartedRoute: WaypointRoute;
  targetPoint: Point;
}

export type RouteAction = ResetAction | InitAction | StartAction | EndAction;

export default function routeReducer(
  routes: RunningRoute[],
  action: RouteAction
) {
  switch (action.type) {
    case "RESET": {
      return [];
    }
    case "INIT": {
      return [...action.routes];
    }
    case "START": {
      const routeToMatch = [
        ...action.unstartedRoute.beenAt.slice(1),
        action.unstartedRoute.waypoint.goingTowards
      ];
      return routes.map(route => {
        const theseRoutes = route.map(waypoint => waypoint.goingTowards);
        if (
          theseRoutes.length !== routeToMatch.length ||
          theseRoutes.some((r, i) => r !== routeToMatch[i])
        ) {
          return route;
        }
        return route.map(waypoint =>
          waypoint.started ? waypoint : { ...waypoint, started: true }
        );
      });
    }
    case "END": {
      const routeToMatch = [
        ...action.unstartedRoute.beenAt.slice(1),
        action.unstartedRoute.waypoint.goingTowards
      ];
      return routes.reduce(
        (a, route) => {
          const theseRoutes = route.map(waypoint => waypoint.goingTowards);
          if (
            theseRoutes.length !== routeToMatch.length ||
            theseRoutes.some((r, i) => r !== routeToMatch[i])
          ) {
            return [...a, route];
          }
          const completedRoute = route.map(r =>
            r.completed ? r : { ...r, completed: true }
          );
          const movingForwards: Distance =
            points[action.unstartedRoute.waypoint.goingTowards];
          const newTargets = Object.keys(movingForwards).filter(
            target => !action.unstartedRoute.beenAt.some(t => target === t)
          );
          if (
            action.unstartedRoute.waypoint.goingTowards ===
              action.targetPoint ||
            newTargets.length === 0
          ) {
            return [...a, completedRoute];
          }
          return [
            ...a,
            ...newTargets.map(newTarget => [
              ...completedRoute,
              {
                goingTowards: newTarget as Point,
                distance: movingForwards[newTarget as Point]!,
                started: false,
                completed: false
              }
            ])
          ];
        },
        [] as RunningRoute[]
      );
    }
    default:
      return routes;
  }
}
