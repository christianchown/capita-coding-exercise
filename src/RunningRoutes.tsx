import React, { useEffect } from "react";
import { RouteAction } from "./routeReducer";
import { RunningRoute, Waypoint } from "./RoutingContext";
import { Point } from "./data";
import findRoutes from "./findRoutes";
import AnimatingRoute, { DURATION } from "./AnimatingRoute";

const unstartedWaypoint = (waypoint: Waypoint) => !waypoint.started;
const uncompletedWaypoint = (waypoint: Waypoint) =>
  waypoint.started && !waypoint.completed;

export default function RunningRoutes({
  startAt,
  endAt,
  routes,
  dispatch,
  size,
  total
}: {
  startAt?: Point;
  endAt?: Point;
  routes: RunningRoute[];
  dispatch: React.Dispatch<RouteAction>;
  size: [number, number];
  total: number;
}) {
  useEffect(() => {
    if (startAt && endAt) {
      findRoutes(startAt, unstartedWaypoint)(routes).forEach(unstartedRoute => {
        dispatch({
          type: "START",
          unstartedRoute
        });
        setTimeout(() => {
          dispatch({ type: "END", unstartedRoute, targetPoint: endAt });
        }, DURATION * 1000 * unstartedRoute.waypoint.distance);
      });
    }
  }, [dispatch, endAt, routes, startAt]);
  if (!startAt) {
    return null;
  }
  const inProgress = findRoutes(startAt, uncompletedWaypoint)(routes);
  return (
    <>
      {inProgress.map(waypointRoute => (
        <AnimatingRoute
          waypointRoute={waypointRoute}
          key={`animating${waypointRoute.beenAt.join("")}${
            waypointRoute.waypoint.goingTowards
          }`}
          size={size}
          total={total}
        />
      ))}
    </>
  );
}
