import { Point, points } from "./data";

export default function getPosition({
  width,
  height,
  pointKey,
  total
}: {
  width: number;
  height: number;
  pointKey: Point;
  total: number;
}) {
  const index = Object.keys(points).findIndex(p => p === pointKey);
  const radians = (2 * Math.PI * (index + total / 2)) / total;
  const radius = (width > height ? height : width) / 2;
  return {
    left: width / 2 + 10 + Math.sin(radians) * radius,
    top: height / 2 + Math.cos(radians) * radius
  };
}
