import React from "react";
import "./Header.scss";

export default function Header() {
  return (
    <header className="header">
      <h1 className="header__title">Capita React Coding Exercise</h1>
    </header>
  );
}
