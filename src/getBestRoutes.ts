import { Point } from "./data";
import { RunningRoute } from "./RoutingContext";
import getRouteLength from "./getRouteLength";

export default function getBestRoutes(
  source: Point,
  destination: Point,
  routes: RunningRoute[]
) {
  if (routes.length === 0) {
    return [];
  }
  const { best } = routes.reduce(
    ({ distance, best }, route) => {
      const routeDistance = getRouteLength(destination, route);
      if (routeDistance === undefined) {
        return { distance, best };
      }
      if (best.length === 0 || routeDistance < distance) {
        return { distance: routeDistance, best: [route] };
      }
      if (routeDistance > distance) {
        return { distance, best };
      }
      return { distance, best: [...best, route] };
    },
    { distance: 0, best: [] } as {
      distance: number;
      best: RunningRoute[];
    }
  );
  return best;
}
