import React, { useState, useCallback } from "react";
import RoutingContext from "./RoutingContext";
import RouteDetail from "./RouteDetail";
import RouteDetails from "./RouteDetails";
import getBestRoutes from "./getBestRoutes";
import getRouteLength from "./getRouteLength";
import "./Instructions.scss";

export default function Instructions({
  busy,
  reset,
  cheeky,
  setCheeky
}: {
  busy: boolean;
  reset: () => void;
  cheeky: boolean;
  setCheeky: (c: boolean) => void;
}) {
  const [cheekyMessage, setCheekyMessage] = useState(false);
  const cheekyButton = useCallback(() => {
    setCheeky(false);
    setCheekyMessage(true);
  }, [setCheeky]);
  const clickReset = useCallback(() => {
    setCheekyMessage(false);
    reset();
  }, [reset]);
  return (
    <RoutingContext.Consumer>
      {({ pointA, pointB, routes }) => {
        if (!pointA) {
          return (
            <div className="instructions">
              <p className="instructions__text">Click on your first point.</p>
            </div>
          );
        }
        if (!pointB) {
          return (
            <div className="instructions">
              <p className="instructions__text">
                Measuring the shortest distance from point{" "}
                <span className="instructions__selected-point">{pointA}</span>.
              </p>
              <p className="instructions__text">
                Now click on your second point.
              </p>
            </div>
          );
        }
        if (!busy) {
          const bestRoutes = getBestRoutes(pointA, pointB, routes);
          return (
            <div className="instructions">
              <p className="instructions__text">Done!</p>
              {bestRoutes.length === 0 && (
                <p className="instructions__text">
                  Oh dear. There are no routes between{" "}
                  <span className="instructions__selected-point">{pointA}</span>{" "}
                  and{" "}
                  <span className="instructions__selected-point">{pointB}</span>
                  .
                </p>
              )}{" "}
              {bestRoutes.length === 1 && (
                <p className="instructions__text">
                  The best route is:{" "}
                  <RouteDetail
                    name="best"
                    start={pointA}
                    route={bestRoutes[0]}
                  />
                  with a distance of{" "}
                  <span className="instructions__distance">
                    {getRouteLength(pointB, bestRoutes[0])}
                  </span>
                </p>
              )}{" "}
              {bestRoutes.length > 1 && (
                <>
                  <p className="instructions__text">The best routes are:</p>
                  <RouteDetails
                    start={pointA}
                    name="best"
                    routes={bestRoutes}
                  />
                </>
              )}
              {bestRoutes.length > 0 && cheeky && !cheekyMessage && (
                <button
                  className="instructions__button"
                  type="button"
                  onClick={cheekyButton}
                >
                  Would you like to view this solution using Dijkstra's
                  algorithm?
                </button>
              )}
              {cheekyMessage && (
                <p className="instructions__cheeky">
                  You best hire me, then{" "}
                  <span role="img" aria-label="Wink wink">
                    😉
                  </span>
                </p>
              )}
              <button
                className="instructions__button"
                type="button"
                onClick={clickReset}
              >
                Click to reset
              </button>
              {!!routes.length && (
                <p className="instructions__text">Here's the routes I tried:</p>
              )}
              <RouteDetails start={pointA} name="all" routes={routes} />
            </div>
          );
        }
        return (
          <div className="instructions">
            <p className="instructions__text">
              Finding the shortest route from{" "}
              <span className="instructions__selected-point">{pointA}</span> to{" "}
              <span className="instructions__selected-point">{pointB}</span>...
            </p>
            <RouteDetails start={pointA} name="all" routes={routes} />
          </div>
        );
      }}
    </RoutingContext.Consumer>
  );
}
