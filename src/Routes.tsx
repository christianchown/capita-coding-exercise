import React, { useRef, useState, useEffect } from "react";
import ResizeObserver from "resize-observer-polyfill";
import { Distances, Point as PointKey } from "./data";
import Point from "./Point";
import Connection from "./Connection";
import RunningRoutes from "./RunningRoutes";
import "./Routes.scss";
import { RunningRoute } from "./RoutingContext";
import { RouteAction } from "./routeReducer";

export default function Routes({
  points,
  pointA,
  pointB,
  clickPoint,
  routes,
  dispatch
}: {
  points: Distances;
  pointA?: PointKey;
  pointB?: PointKey;
  clickPoint?: (point: PointKey) => void;
  routes: RunningRoute[];
  dispatch: React.Dispatch<RouteAction>;
}) {
  const pointKeys = Object.keys(points) as PointKey[];
  const container = useRef<HTMLDivElement>(null);
  const [size, setSize] = useState<[number, number]>([0, 0]);
  useEffect(() => {
    const resizeObserver = new ResizeObserver(entries => {
      const node = entries[0].target;
      setSize([node.clientWidth - 60, node.clientHeight]);
    });
    if (container.current) {
      resizeObserver.observe(container.current);
    }
    return resizeObserver.disconnect;
  }, []);
  return (
    <div className="routes" ref={container}>
      {pointKeys.map(pointKey => (
        <React.Fragment key={pointKey}>
          <Point
            pointKey={pointKey}
            total={pointKeys.length}
            size={size}
            clickPoint={
              pointKey === pointA || pointKey === pointB
                ? undefined
                : clickPoint
            }
            selected={pointKey === pointA || pointKey === pointB}
          />
          {(Object.keys(points[pointKey]) as PointKey[])
            .filter(
              connection =>
                connection.toString().charCodeAt(0) >
                pointKey.toString().charCodeAt(0)
            )
            .map(connection => (
              <Connection
                key={`${pointKey}→${connection}`}
                name={`${pointKey}→${connection}`}
                source={pointKey}
                dest={connection}
                total={pointKeys.length}
                size={size}
                distance={points[pointKey][connection]!}
              />
            ))}
        </React.Fragment>
      ))}
      <RunningRoutes
        startAt={pointA}
        endAt={pointB}
        routes={routes}
        dispatch={dispatch}
        size={size}
        total={pointKeys.length}
      />
    </div>
  );
}
