import getRouteLength from "./getRouteLength";

it("returns undefined if there's no routes", () => {
  expect(getRouteLength("a", [])).toBe(undefined);
});

it("returns undefined if it doesn't end at the destination", () => {
  expect(
    getRouteLength("a", [
      { goingTowards: "c", started: true, completed: true, distance: 10 }
    ])
  ).toBe(undefined);
});

it("returns the distance if it ends at the destination", () => {
  expect(
    getRouteLength("a", [
      { goingTowards: "c", started: true, completed: true, distance: 10 },
      { goingTowards: "a", started: true, completed: true, distance: 10 }
    ])
  ).toBe(20);
});
