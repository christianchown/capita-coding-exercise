import React, { useMemo } from "react";
import getPosition from "./getPosition";
import { WaypointRoute } from "./findRoutes";
import "./AnimatingRoute.scss";

export const DURATION = 0.5;

export default function AnimatingRoute({
  waypointRoute,
  size: [width, height],
  total
}: {
  waypointRoute: WaypointRoute;
  size: [number, number];
  total: number;
}) {
  const memoised = useMemo(() => {
    const startPosition = getPosition({
      width,
      height,
      pointKey: waypointRoute.beenAt[waypointRoute.beenAt.length - 1],
      total
    });
    const endPosition = getPosition({
      width,
      height,
      pointKey: waypointRoute.waypoint.goingTowards,
      total
    });
    const diffLeft = endPosition.left - startPosition.left;
    const diffTop = endPosition.top - startPosition.top;
    const animationName = `animate-${waypointRoute.beenAt.join("")}${
      waypointRoute.waypoint.goingTowards
    }-${Math.round(diffLeft)}-${Math.round(diffTop)}`;
    const keyframes = `@keyframes ${animationName} {
      0% {
        transform: translate(0, 0);
      }
      100% {
        transform: translate(${diffLeft}px, ${diffTop}px);
      }
    }`;
    const keyframeStyle = document.createElement("style");
    keyframeStyle.type = "text/css";
    keyframeStyle.appendChild(document.createTextNode(keyframes));
    document.getElementsByTagName("head")[0].appendChild(keyframeStyle);

    return (
      <div
        className="animating-route"
        key={`from${waypointRoute.beenAt.join("")}to${
          waypointRoute.waypoint.goingTowards
        }`}
        style={{
          position: "absolute",
          animationName,
          animationTimingFunction: "linear",
          animationDuration: `${DURATION * waypointRoute.waypoint.distance}s`,
          animationDelay: "0.0s",
          animationIterationCount: 1,
          animationDirection: "normal",
          animationFillMode: "forwards",
          ...startPosition
        }}
      />
    );
  }, [
    width,
    height,
    waypointRoute.beenAt,
    waypointRoute.waypoint.goingTowards,
    waypointRoute.waypoint.distance,
    total
  ]);
  return memoised;
}
