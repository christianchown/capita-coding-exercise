const oneWayPoints = {
  a: {
    c: 2
  },
  c: {
    d: 1,
    f: 4
  },
  b: {
    d: 4,
    e: 7
  },
  d: {
    f: 1,
    g: 2
  },
  f: {
    g: 3
  },
  g: {
    h: 4
  },
  e: {
    h: 10
  },
  h: {}
};

export const points = Object.keys(oneWayPoints).reduce(
  (a, point) => {
    const destPoints = oneWayPoints[point as keyof typeof oneWayPoints];
    Object.keys(destPoints).forEach(dest => {
      a[dest as keyof typeof a] = {
        ...(a[dest as keyof typeof destPoints] || {}),
        [point]:
          oneWayPoints[point as keyof typeof oneWayPoints][
            dest as keyof typeof destPoints
          ]
      };
    });
    return a;
  },
  oneWayPoints as {
    [source: string]: {
      [dest: string]: number;
    };
  }
);

export type Point = keyof typeof points;
export type Distance = {
  [dest in Point]?: number;
};
export type Distances = {
  [source in Point]: Distance;
};
