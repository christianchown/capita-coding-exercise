import { Point } from "./data";
import { RunningRoute } from "./RoutingContext";

export default function getRouteLength(
  destination: Point,
  route: RunningRoute
): number | undefined {
  if (route.length === 0) {
    return undefined;
  }
  if (route[route.length - 1].goingTowards !== destination) {
    return undefined;
  }
  return route.reduce((total, waypoint) => {
    return total + waypoint.distance;
  }, 0);
}
