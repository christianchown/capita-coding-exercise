import React from "react";
import { Point } from "./data";

export interface Waypoint {
  goingTowards: Point;
  distance: number;
  started: boolean;
  completed: boolean;
}

export type RunningRoute = Waypoint[];

export const defaultOptions: {
  pointA?: Point;
  pointB?: Point;
  routes: RunningRoute[];
} = {
  pointA: undefined,
  pointB: undefined,
  routes: []
};
export default React.createContext(defaultOptions);
