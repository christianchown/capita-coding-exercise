import React from "react";
import { RunningRoute } from "./RoutingContext";
import { Point } from "./data";
import "./RouteDetail.scss";

export default function RouteDetail({
  start,
  route,
  name
}: {
  start: Point;
  route: RunningRoute;
  name: string;
}) {
  return (
    <span className="route-detail">
      <span className="route-detail__point">{start}</span>
      {route.map(waypoint => (
        <React.Fragment key={`${name}${waypoint.goingTowards}`}>
          {" "}
          → <span className="route-detail__point">
            {waypoint.goingTowards}
          </span>{" "}
          <span className="route-detail__distance">{waypoint.distance}</span>
        </React.Fragment>
      ))}
    </span>
  );
}
