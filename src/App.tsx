import React, { useState, useCallback, useReducer, useEffect } from "react";
import "./App.scss";
import { points, Point as PointKey, Distance } from "./data";
import Header from "./Header";
import Routes from "./Routes";
import Instructions from "./Instructions";
import RoutingContext, { Waypoint } from "./RoutingContext";
import routeReducer from "./routeReducer";
import findRoutes from "./findRoutes";

const incompleteWaypoint = (route: Waypoint) => !route.completed;

export default function App() {
  const [pointA, setPointA] = useState(undefined as undefined | PointKey);
  const [pointB, setPointB] = useState(undefined as undefined | PointKey);
  const [routes, dispatch] = useReducer(routeReducer, []);
  const [busy, setBusy] = useState(false);
  const [cheeky, setCheeky] = useState(true);

  const clickPoint = useCallback(
    async (clicked: PointKey) => {
      if (!pointA) {
        setPointA(clicked);
      } else if (!pointB) {
        setPointB(clicked);
        setBusy(true);
        const startingRoutes = Object.keys(points[pointA]) as PointKey[];
        if (startingRoutes.length === 0) {
          setBusy(false);
        } else {
          dispatch({
            type: "INIT",
            routes: startingRoutes.map(destination => [
              {
                goingTowards: destination,
                distance: (points[pointA] as Distance)[destination]!,
                completed: false,
                started: false
              }
            ])
          });
        }
      }
    },
    [pointA, pointB]
  );
  const reset = useCallback(() => {
    setPointA(undefined);
    setPointB(undefined);
    dispatch({ type: "RESET" });
    setBusy(false);
  }, []);

  useEffect(() => {
    if (busy && routes.length && pointA) {
      const incompleteRoutes = findRoutes(pointA, incompleteWaypoint)(routes);
      if (incompleteRoutes.length === 0) {
        setBusy(false);
      }
    }
  }, [busy, pointA, routes]);
  return (
    <RoutingContext.Provider value={{ pointA, pointB, routes }}>
      <article className="app">
        <section className="app__header">
          <Header />
        </section>
        <section className="app__body">
          <Routes
            points={points}
            pointA={pointA}
            pointB={pointB}
            clickPoint={busy || (pointA && pointB) ? undefined : clickPoint}
            routes={routes}
            dispatch={dispatch}
          />
        </section>
        <section className="app__info">
          <Instructions
            busy={busy}
            reset={reset}
            cheeky={cheeky}
            setCheeky={setCheeky}
          />
        </section>
      </article>
    </RoutingContext.Provider>
  );
}
