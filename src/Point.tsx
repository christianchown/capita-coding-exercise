import React, { CSSProperties, useCallback } from "react";
import { Point as PointKey } from "./data";
import getPosition from "./getPosition";
import "./Point.scss";

function ButtonOrDiv({
  onClick,
  selected,
  clickable,
  style,
  children
}: {
  onClick: () => void;
  selected: boolean;
  clickable: boolean;
  style: CSSProperties;
  children: React.ReactNode;
}) {
  if (selected) {
    return (
      <div className="point point--selected" style={style}>
        {children}
      </div>
    );
  }
  if (!clickable) {
    return (
      <div className="point point--inactive" style={style}>
        {children}
      </div>
    );
  }
  return (
    <button className="point" onClick={onClick} style={style}>
      {children}
    </button>
  );
}

export default function Point({
  pointKey,
  total,
  size: [width, height],
  clickPoint,
  selected
}: {
  pointKey: PointKey;
  total: number;
  size: [number, number];
  clickPoint?: (point: PointKey) => void;
  selected: boolean;
}) {
  const style: CSSProperties = {
    position: "absolute",
    ...getPosition({ width, height, pointKey, total })
  };
  const onClick = useCallback(() => {
    if (clickPoint) {
      clickPoint(pointKey);
    }
  }, [clickPoint, pointKey]);
  if (!width || !height) {
    return null;
  }
  return (
    <ButtonOrDiv
      selected={selected}
      clickable={!!clickPoint}
      style={style}
      onClick={onClick}
    >
      <div className="point__title">{pointKey}</div>
    </ButtonOrDiv>
  );
}
