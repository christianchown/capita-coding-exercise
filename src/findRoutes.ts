import { RunningRoute, Waypoint } from "./RoutingContext";
import { Point } from "./data";

export interface WaypointRoute {
  beenAt: Point[];
  waypoint: Waypoint;
}

export default function findRoutes(
  startAt: Point,
  condition: (waypoint: Waypoint) => boolean
) {
  return function findRoutesWhere(routes: RunningRoute[]) {
    return routes.reduce(
      (a, route) => {
        return route.reduce((b, waypoint, i) => {
          if (condition(waypoint)) {
            b.push({
              beenAt: [
                startAt,
                ...(i === 0
                  ? []
                  : route.slice(0, i).map(waypoint => waypoint.goingTowards))
              ],
              waypoint
            });
          }
          return b;
        }, a);
      },
      [] as WaypointRoute[]
    );
  };
}
