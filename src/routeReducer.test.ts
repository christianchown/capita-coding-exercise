import routeReducer from "./routeReducer";
import { Waypoint } from "./RoutingContext";
import { WaypointRoute } from "./findRoutes";

const waypointA: Waypoint = {
  goingTowards: "a",
  distance: 1,
  started: false,
  completed: false
};

const waypointB: Waypoint = {
  goingTowards: "b",
  distance: 1,
  started: true,
  completed: true
};

const waypointC: Waypoint = {
  goingTowards: "c",
  distance: 3,
  started: false,
  completed: false
};

it("initialises", () => {
  expect(routeReducer([], { type: "INIT", routes: [[waypointA]] })).toEqual([
    [waypointA]
  ]);
});

it("resets", () => {
  expect(routeReducer([[waypointA]], { type: "RESET" })).toEqual([]);
});

it("starts", () => {
  const startingRoutes = [[waypointA], [waypointB, waypointC]];
  const expectedRoutes = [
    [waypointA],
    [waypointB, { ...waypointC, started: true }]
  ];
  expect(
    routeReducer(startingRoutes, {
      type: "START",
      unstartedRoute: { beenAt: ["a", "b"], waypoint: waypointC }
    })
  ).toEqual(expectedRoutes);
});

it("ends at the target point", () => {
  const startingRoutes = [
    [waypointA],
    [waypointB, { ...waypointC, started: true }]
  ];
  const expectedRoutes = [
    [waypointA],
    [waypointB, { ...waypointC, started: true, completed: true }]
  ];

  expect(
    routeReducer(startingRoutes, {
      type: "END",
      unstartedRoute: { beenAt: ["a", "b"], waypoint: waypointC },
      targetPoint: "c"
    })
  ).toEqual(expectedRoutes);
});

it("continues if not at the target point", () => {
  const targetPoint = "h";
  const startingRoutes = [
    [waypointA],
    [waypointB, { ...waypointC, started: true }]
  ];
  const currentRoute = [
    waypointB,
    { ...waypointC, started: true, completed: true }
  ];
  const unstartedRoute: WaypointRoute = {
    beenAt: ["a", "b"],
    waypoint: waypointC
  };
  const expectedRoutes = [
    [waypointA],
    [
      ...currentRoute,
      { goingTowards: "d", distance: 1, started: false, completed: false }
    ],
    [
      ...currentRoute,
      { goingTowards: "f", distance: 4, started: false, completed: false }
    ]
  ];

  expect(
    routeReducer(startingRoutes, {
      type: "END",
      unstartedRoute,
      targetPoint
    })
  ).toEqual(expectedRoutes);
});

it("does not loop back on itself", () => {
  const targetPoint = "h";
  const waypointD: Waypoint = { ...waypointB, goingTowards: "d" };
  const startingRoutes = [
    [waypointA],
    [waypointD, { ...waypointC, started: true }]
  ];
  const currentRoute = [
    waypointD,
    { ...waypointC, started: true, completed: true }
  ];
  const unstartedRoute: WaypointRoute = {
    beenAt: ["a", "d"],
    waypoint: waypointC
  };
  const expectedRoutes = [
    [waypointA],
    [
      ...currentRoute,
      { goingTowards: "f", distance: 4, started: false, completed: false }
    ]
  ];

  expect(
    routeReducer(startingRoutes, {
      type: "END",
      unstartedRoute,
      targetPoint
    })
  ).toEqual(expectedRoutes);
});
