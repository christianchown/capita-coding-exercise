import React from "react";
import { RunningRoute } from "./RoutingContext";
import RouteDetail from "./RouteDetail";
import { Point } from "./data";

export default function RouteDetails({
  name,
  start,
  routes
}: {
  name: string;
  start: Point;
  routes: RunningRoute[];
}) {
  if (routes.length === 0) {
    return null;
  }
  return (
    <ul className="route-details">
      {routes.map((route, i) => (
        <li key={`${name}${i}`} className="route-details__detail">
          <RouteDetail name={`${name}${i}`} start={start} route={route} />
        </li>
      ))}
    </ul>
  );
}
